package suitebot2.ai;

import suitebot2.game.GameRound;
import suitebot2.game.GameSetup;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by nvispute on 6/22/2016.
 */
public class TurtleBotAi implements BotAi {
    public Game game;
    Queue<String> moves = makeInitialSquare();
    int square = 0;

    @Override
    public String initializeAndMakeMove(GameSetup gameSetup) {
        game = new Game(gameSetup);
        return moves.poll();
    }

    public Queue<String> makeInitialSquare() {
        Queue<String> square = new LinkedList<>();
        for (int i = 0; i < 4; i++) { square.add("U"); }
        for (int i = 0; i < 4; i++) { square.add("L"); }
        for (int i = 0; i < 7; i++) { square.add("D"); }
        for (int i = 0; i < 7; i++) { square.add("R"); }
        for (int i = 0; i < 7; i++) { square.add("U"); }
        for (int i = 0; i < 3; i++) { square.add("L"); }
        return square;
    }

    @Override
    public String makeMove(GameRound gameRound) {
        try {
            game.move++;
            //game.syncLocal(gameRound);
            if (game.move > 60) {
                return moveZigZag();
            }
            if (moves.isEmpty()) {
                moves = game.makeNextSquare();
            }

            return moves.poll();

//            return moveZigZag();
        } catch (Throwable t) {t.printStackTrace(System.err);}
        return moveZigZag();
    }

    public String moveZigZag() {
        if (game.move % 2 == 0) {
            return moveDown();
        }

        return moveRight();
    }

    public String moveRight() {
        return "R";
    }

    public String moveLeft() {
        return "L";
    }

    public String moveDown() {
        return "D";
    }

    public String moveUp() {
        return "U";
    }

    public String handleCollision() {
        if (game.getX() != 0) {
            return moveDown();
        } else {
            return moveRight();
        }
    }
}

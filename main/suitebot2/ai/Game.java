package suitebot2.ai;

import suitebot2.game.GameRound;
import suitebot2.game.GameSetup;
import suitebot2.game.PlayerMove;
import suitebot2.game.Point;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by nvispute on 6/23/2016.
 */
public class Game {
    private int myId;
    public final int width;
    public final int height;
    public final boolean taken[][];
    public final Point[] points;
    public int move = 0;
    private Direction direction = new Direction();

    public Game(GameSetup gameSetup) {
        myId = gameSetup.aiPlayerId;
//        width = Math.abs(gameSetup.gamePlan.width);
//        height = Math.abs(gameSetup.gamePlan.height);
        width = gameSetup.gamePlan.width;
        height = gameSetup.gamePlan.height;
        taken = new boolean[width][height];
        points = gameSetup.gamePlan.startingPositions.toArray(new Point[gameSetup.gamePlan.startingPositions.size()]);

        syncPoints();

    }

    public boolean collided(GameRound gameRound) {
        return gameRound.moves.get(myId).move.equals("X");
    }

    public boolean hitX() {
        return getX() >= width - 1;
    }

    void syncLocal(GameRound gameRound) {
        syncPositions(gameRound.moves);
    }

    public int getX() {
        return points[myId].x;
    }

    public int getY() {
        return points[myId].y;
    }

    void syncPositions(List<PlayerMove> moves) {
        for (PlayerMove move : moves) {
            String moveString = move.move;
            if ("U".equals(moveString)) {
                points[move.playerId].y--;
                if (points[move.playerId].y < 0) {
                    points[move.playerId].y = height - 1;
                }
            }
            if ("D".equals(moveString)) {
                points[move.playerId].y++;
                if (points[move.playerId].y >= height) {
                    points[move.playerId].y = 0;
                }
            }
            if ("L".equals(moveString)) {
                points[move.playerId].x--;
                if (points[move.playerId].x < 0) {
                    points[move.playerId].x = width - 1;
                }
            }
            if ("R".equals(moveString)) {
                points[move.playerId].x++;
                if (points[move.playerId].x >= width) {
                    points[move.playerId].x = 0;
                }
            }
        }
        syncPoints();
    }

    void syncPoints() {
        for (Point point : points) {
            taken[point.x][point.y] = true;
        }
    }

    int squareSize = 8;

    Queue<String> makeNextSquare() {
        Queue<String> square = new LinkedList<>();
        String nextDirection = direction.nextDirection();
        for (int i = 0; i < squareSize - 2; i++) { square.add(nextDirection); }
        nextDirection = direction.nextDirection();
        for (int i = 0; i < squareSize - 3; i++) { square.add(nextDirection); }
        nextDirection = direction.nextDirection();
        for (int i = 0; i < squareSize - 5; i++) { square.add(nextDirection); }
        nextDirection = direction.nextDirection();
        nextDirection = direction.nextDirection();
//        for (int i = 0; i < squareSize - 4; i++) { square.add(nextDirection); }
        squareSize--;
        squareSize = squareSize < 5 ? 5 : squareSize;
        return square;
    }
}

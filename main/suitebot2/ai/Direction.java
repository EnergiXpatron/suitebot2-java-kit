package suitebot2.ai;

import java.util.*;

/**
 * Created by nvispute on 6/23/2016.
 */
public class Direction {
    final Queue<String> directQueue = new LinkedList<>();
    public Direction() {

        directQueue.add("U");
        directQueue.add("R");
        directQueue.add("D");
        directQueue.add("R");

        directQueue.add("U");
        directQueue.add("L");
        directQueue.add("D");
        directQueue.add("R");
        directQueue.add("U");
        directQueue.add("L");
        directQueue.add("D");
        directQueue.add("R");
        directQueue.add("U");
        directQueue.add("L");
        directQueue.add("D");
        directQueue.add("R");
        directQueue.add("U");
        directQueue.add("L");
        directQueue.add("D");
        directQueue.add("R");
    }

    public String nextDirection() {
        String nextDirection = directQueue.poll();
        directQueue.add(nextDirection);
        return nextDirection;
    }
}

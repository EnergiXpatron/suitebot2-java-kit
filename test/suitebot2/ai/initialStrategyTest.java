package suitebot2.ai;

import org.junit.Test;
import suitebot2.game.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by nvispute on 6/23/2016.
 */
public class initialStrategyTest {

    @Test
    public void initialStrategyTest() {
        TurtleBotAi turtleBotAi = new TurtleBotAi();
        List<Point> startingPositions = new ArrayList<>();
        startingPositions.add(new Point(0, 0));
        turtleBotAi.initializeAndMakeMove(new GameSetup(0, new ArrayList<>(), new GamePlan(150, 30, startingPositions, 0)));
        String playerMove = "R";
        for (int i = 0; i < 10000; i++) {
            List<PlayerMove> playerMoveList = new ArrayList<>();
            playerMoveList.add(new PlayerMove(0, playerMove));
            playerMove = turtleBotAi.makeMove(new GameRound(playerMoveList));
            System.out.println(playerMove);
        }
    }

    @Test
    public void initialStrategyCollisionTest() {
        TurtleBotAi turtleBotAi = new TurtleBotAi();
        List<Point> startingPositions = new ArrayList<>();
        startingPositions.add(new Point(0, 0));
        turtleBotAi.initializeAndMakeMove(new GameSetup(0, new ArrayList<>(), new GamePlan(150, 30, startingPositions, 0)));
        String playerMove = "R";
        for (int i = 1; i < 10000; i++) {
            List<PlayerMove> playerMoveList = new ArrayList<>();
            if (i % 18 == 0) {
                playerMove = "X";
            }
            playerMoveList.add(new PlayerMove(0, playerMove));
            playerMove = turtleBotAi.makeMove(new GameRound(playerMoveList));
            System.out.println(playerMove);
        }
    }

    @Test
    public void manyPlayerMoveTest() {
        BotAi left = new BotAi() {
            @Override
            public String initializeAndMakeMove(GameSetup gameSetup) {
                return "L";
            }

            @Override
            public String makeMove(GameRound gameRound) {
                return "L";
            }
        };
        TurtleBotAi turtleBotAi = new TurtleBotAi();
        List<Point> startingPositions = new ArrayList<>();
        int count = 1;
        for (int i = 0; i < 18; i++) {
            startingPositions.add(new Point(count, 5 * count));
            count ++;
        }
        String playerMove = turtleBotAi.initializeAndMakeMove(new GameSetup(0, Arrays.asList(new Player(0, "turtle"), new Player(1, "left")), new GamePlan(100, 100, startingPositions, 0)));
        for (int i = 0; i < 10000; i++) {
            List<PlayerMove> playerMoveList = new ArrayList<>();
            playerMoveList.add(new PlayerMove(0, playerMove));
            playerMoveList.add(new PlayerMove(1, "L"));
            playerMove = turtleBotAi.makeMove(new GameRound(playerMoveList));
            System.out.println(playerMove);
        }
    }
}
